# Digital Research Academy Train the Trainer (TTT) program

Welcome to the repository of our Train the Trainer program :wave:!

For details on the program, please see https://digital-research.academy/train-the-trainer/.


## Checklist

To become a DRA trainer, in the TTT pilot program, each trainer needs to fulfill
the following requirements:

### Part 1:

- [ ] Participated in (almost) all didactics sessions
- [ ] Taught one of the didactics lessons

### Part 2:

- [ ] Served as reviewer of at least two courses of peers
- [ ] Prepared and held one course
- [ ] Worked in the feedback of reviewers into the course
- [ ] Published the course as part of the Digital Research Academy resource
  collection

### Create a checklist to track your progress:

- Go to https://gitlab.com/digital-research-academy/dra-train-the-trainer/-/issues
- Click **New issue**
- Choose the **TTT checklist** template
- Name the issue "TTT: firstname lastname"

This way you know what you still need to do and we know your status and can 
determine your needs better. :raised_hands: 

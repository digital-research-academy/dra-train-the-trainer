<!-- Name the issue "TTT: firstname lastname" -->

Check off the tasks once you have accomplished them:

### Part 1:

- [ ] Participated in (almost) all didactics sessions
- [ ] Taught one of the didactics lessons

### Part 2:

- [ ] Served as reviewer of at least two courses of peers
  - [ ] Course 1
  - [ ] Course 2
- [ ] Prepared and held one course
- [ ] Worked in the feedback of reviewers into the course
- [ ] Published the course as part of the Digital Research Academy resource
  collection

Once you check all the boxes you are an official trainer of the Digital 
Research Academy.